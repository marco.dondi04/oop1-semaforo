package com.company;

public class Main {

    public static void main(String[] args) {

        Semaforo semaforo = new Semaforo();
        Semaforo semaforo2 = new Semaforo();

        semaforo.suono = "assillante";
        semaforo.altezza = 4.5;
        semaforo.disposizione = "orizzontale";
        semaforo.timer  = 3.5;
        semaforo.transito = "pedoni";

        System.out.println(semaforo);

        semaforo.output_luce("rosso");
        semaforo2.suono = "tranquillo";
        semaforo2.altezza = 5.4;
        semaforo2.disposizione = "verticale";
        semaforo2.timer  = 1.5;
        semaforo2.transito = "biciclette";

        System.out.println(semaforo2);
        semaforo2.output_luce("verde");


    }
}
