package com.company;

public class Semaforo
{
    String suono;
    String disposizione;
    String transito;
    double timer;
    double altezza;

    public String toString(){return "sono un semaforo con un suono molto " + suono + ", sono disposto in " + disposizione + ", sono alto " + altezza + " metri, cambio colore ogni " + timer + " minuti e sono riservato esculivamente a " + transito;}
    public void output_luce(String colore){System.out.println("molto spesso sono di colore " + colore);}

}

